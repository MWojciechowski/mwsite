﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MWMain.Models;
using MWMain.Models.Recognition.YoloParser;
using MWMain.Models.Recognition;
using Microsoft.ML;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace MWMain.Controllers
{
    [Authorize]
    public class ImageController : Controller
    {
        private readonly ImageDbContext _context;
        private readonly IWebHostEnvironment _hostEnvironment;

        public ImageController(ImageDbContext context, IWebHostEnvironment hostEnvironment)
        {
            _context = context;
            this._hostEnvironment = hostEnvironment;
        }

        // GET: Image
        public async Task<IActionResult> Index()
        {
            return View(await _context.Images.ToListAsync());
        }

        // GET: Image/Analyze/5
        public async Task<IActionResult> Analyze(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var imageModel = await _context.Images
                .FirstOrDefaultAsync(m => m.ImageId == id);
            
            if (imageModel == null)
            {
                return NotFound();
            }

            ViewBag.AltImageSource = false;
                //CheckIfAltImageExists(imageModel.ImageName);

            return View(imageModel);
        }

        // POST: Image/Analyze/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Analyze(int id)
        {
            ViewData["Test"] = true;
            var imageModel = await _context.Images
            .FirstOrDefaultAsync(m => m.ImageId == id);
            if (imageModel == null)
                    return NotFound();

            if (ModelState.IsValid)
            {
                string wwwRootPath = _hostEnvironment.WebRootPath;
                var baseFileLocation = $"{wwwRootPath}/Image/{imageModel.ImageName}";
                var assetsRelativePath = $"{wwwRootPath}/Image/TrainingData";
                string assetsPath = RecognitionHelpers.GetAbsolutePath(assetsRelativePath);
                var modelFilePath = Path.Combine(assetsPath, "TinyYolo2_model.onnx");
                var imageLocalization = Path.Combine($"{wwwRootPath}/Image/");
                var outputFolder = Path.Combine(assetsPath, "output");
                MLContext mlContext = new MLContext();

                try
                {
                    var image = ImageNetData.ReadFromFile($"{imageLocalization}").Where(i => i.ImagePath == baseFileLocation);
                    IDataView imageDataView = mlContext.Data.LoadFromEnumerable(image);
                    var modelScorer = new OnnxModelScorer(imageLocalization, modelFilePath, mlContext);

                    IEnumerable<float[]> probabilities = modelScorer.Score(imageDataView);
                    YoloOutputParser parser = new YoloOutputParser();

                    var boundingBoxes = probabilities
                        .Select(probability => parser.ParseOutputs(probability))
                        .Select(boxes => parser.FilterBoundingBoxes(boxes, 5, .5F));

                    string imageFileName = image.FirstOrDefault().Label;
                    IList<YoloBoundingBox> detectedObjects = boundingBoxes.FirstOrDefault();

                    RecognitionHelpers.DrawBoundingBox(imageLocalization, outputFolder, imageFileName, detectedObjects);
                    RecognitionHelpers.LogDetectedObjects(imageFileName, detectedObjects);
                    Console.WriteLine("========= End of Process ========");

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            ViewBag.AltImageSource = CheckIfAltImageExists(imageModel.ImageName);

            return View(imageModel);
        }

            // GET: Image/Create
            public IActionResult Create()
        {
            return View();
        }

        // POST: Image/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ImageId,Title,ImageFile")] ImageModel imageModel)
        {
            if (ModelState.IsValid)
            {
                #region SaveImage to wwwRoot
                string wwwRootPath = _hostEnvironment.WebRootPath;
                string fileName = Path.GetFileNameWithoutExtension(imageModel.ImageFile.FileName);
                string extension = Path.GetExtension(imageModel.ImageFile.FileName);
                imageModel.ImageName = fileName = $"{fileName}_{DateTime.Now.ToString("yymmss")}{extension}";
                string path = Path.Combine($"{wwwRootPath}/Image/", fileName);

                using (var fileStream = new FileStream(path, FileMode.Create))
                {
                    await imageModel.ImageFile.CopyToAsync(fileStream);
                }
                #endregion

                _context.Add(imageModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(imageModel);
        }

        // GET: Image/Analyze/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var imageModel = await _context.Images.FindAsync(id);
            if (imageModel == null)
            {
                return NotFound();
            }
            return View(imageModel);
        }
        
        // GET: Image/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var imageModel = await _context.Images.FindAsync(id);
            if (imageModel == null)
            {
                return NotFound();
            }
            return View(imageModel);
        }

        // POST: Image/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ImageId,Title,ImageName")] ImageModel imageModel)
        {
            if (id != imageModel.ImageId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(imageModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ImageModelExists(imageModel.ImageId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(imageModel);
        }

        // GET: Image/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var imageModel = await _context.Images
                .FirstOrDefaultAsync(m => m.ImageId == id);
            if (imageModel == null)
            {
                return NotFound();
            }

            return View(imageModel);
        }

        // POST: Image/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var imageModel = await _context.Images.FindAsync(id);
            #region Remove from wwwroot/Image
            var imagePath = Path.Combine(_hostEnvironment.WebRootPath, "image", imageModel.ImageName);
            if (System.IO.File.Exists(imagePath))
                System.IO.File.Delete(imagePath);


            #endregion

            _context.Images.Remove(imageModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CheckIfAltImageExists(string imgName)
        {
            string wwwRootPath = _hostEnvironment.WebRootPath;
            if (System.IO.File.Exists($"{wwwRootPath}/Image/TrainingData/output/{imgName}"))
                return true;
            return false;
        }

        private bool ImageModelExists(int id)
        {
            return _context.Images.Any(e => e.ImageId == id);
        }
    }
}
