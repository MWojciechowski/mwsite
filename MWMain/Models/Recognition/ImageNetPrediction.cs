﻿using Microsoft.ML.Data;

namespace MWMain.Models.Recognition
{
    public class ImageNetPrediction
    {
        [ColumnName("grid")]
        public float[] PredictedLabels;
    }
}
